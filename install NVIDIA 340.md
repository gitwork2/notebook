## Установка драйвера NVIDIA на Linux.

В статье описано решение проблемы с устновкой драйвера NVIDIA 340 на Linux.

Снчало узнайте требующийся вам драйвер командой:

    ubuntu-drivers devices
Проблема устновки возникает из-за некоректно установленного до этого драйвера, поэтому удалим все файлы nvidia.

Выполняйте эти команды по порядку в терминале:

    sudo apt-get remove --purge '^nvidia-.*'
    sudo rm /etc/X11/xorg.conf
    echo 'nouveau' | sudo tee -a /etc/modules

Потом устанавливаем **НЕ** оригинльный PPA драйвер:

    sudo add-apt-repository ppa:kelebek333/nvidia-legacy
    sudo apt update

После перезгрузки компьютера выберете драйвер в графической утилите.

**Если** у вас некорекно работают программы для 3d/2d моделирования (FreeCAD), выполните:

    sudo apt-get install nvidia-390

Удачи!

[Исходник.](https://forum.ubuntu.ru/index.php?topic=313974.0)