## Создание aliases на Linux

Создание alias на Linux для альтернативных команд.

Alias - это команда ссылающаяся на другую команду.

Заходим в папку с aliases:


    micro ~/.bashrc
Ищем строку с aliases и добавляем свой:

    alias псевдоним="любая комнда"
Выходи из редктора F4 с сохранением в редакторе и системе:

    source ~/.bashrc
[Исходник](https://itisgood.ru/2018/10/03/kak-sozdat-i-ispolzovat-alias-komandy-v-linux/)
